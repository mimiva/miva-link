import { createApp } from 'vue'
import App from './App.vue'
import Store from './store'

import 'normalize.css'

createApp(App).use(Store).mount('#app')
