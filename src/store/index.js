import { createStore } from 'vuex'

export default createStore({
    state() {
        return {
            isSideOpen: true,
        }
    },
    mutations: {
        switchSide(state) {
            state.isSideOpen = !state.isSideOpen;
        }
    },
});